# conda install scipy=1.2.1
# conda install opencv=4.1

import glob
import cv2
import numpy as np
import cvHelper
from scipy.spatial.transform import Rotation as R

# read in hand positions
poseFiles=glob.glob("data2/poses/*.txt")
poseFiles.sort()

gripperRvecs=[]
gripperTvecs=[]
for p in poseFiles:
    m=np.loadtxt(p)
    gripperRvecs.append(m[0:3, 0:3])
    tvec = m[0:3,3]
    gripperTvecs.append(tvec.T/1000)

imgFiles=glob.glob("data2/imgs/*.png")
imgFiles.sort()

# camera positions
imgRvecs,imgTvecs = [],[]

#for file in imgFiles:
#...
