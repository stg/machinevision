import cv2
from cv2 import aruco
import cvHelper

img = cv2.imread('aruco_markers.jpg',0)
aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
parameters =  aruco.DetectorParameters_create()

corners, ids, _ = aruco.detectMarkers(img, aruco_dict, 
                                      parameters=parameters)

imgc = aruco.drawDetectedMarkers(cv2.cvtColor(img,cv2.COLOR_GRAY2RGB), 
                                 corners, ids)
cvHelper.image(imgc)

