import cv2
import os

# create image if it does not exist
dirname="./imgs/"
if not os.path.exists(dirname):
    os.makedirs(dirname)

# create video capture
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_AUTOFOCUS, 0) # turn the autofocus off
cap.set(cv2.CAP_PROP_SETTINGS, 1);

i=0
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    img_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame',img_gray)
    key=cv2.waitKey(1) & 0xFF
    if  key == ord('q'):
        break
    elif key == ord('s'):
        imgname="{0}image_{1}.png".format(dirname,i)
        cv2.imwrite(imgname, img_gray)
        print("saving to "+imgname)      
        i+=1

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()