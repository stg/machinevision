#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 21:09:43 2019

"""

import numpy as np
import cv2 as cv
import glob
import cvHelper

cbrow = 12
cbcol = 11


# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
world_points = np.zeros((cbrow * cbcol, 3), np.float32)
world_points[:, :2] = np.mgrid[0:cbcol, 0:cbrow].T.reshape(-1, 2)

# Arrays to store object points and image points from all the images.
list_3d_points = [] # 3d point in real world space
list_2d_points = [] # 2d points in image plane.
images = glob.glob('./calib_example/*.tif')
img_size=[]

for fname in images:
    img = cv.imread(fname,0)
    img_size=img.shape[::-1]
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(img,  (cbcol, cbrow))
    # If found, add object points, image points (after refining them)
    if ret:
        list_3d_points.append(world_points)
        list_2d_points.append(corners)

        # Draw and display the corners
        img_color=cv.cvtColor(img,cv.COLOR_GRAY2RGB)
        cv.drawChessboardCorners(img_color, (cbcol, cbrow), corners, ret)        
        cvHelper.image(img_color, fname)

#------------------------------------------------------------------------------
## calibrate Camera
...

for fname in images:
    img = cv.imread(fname,0)
    # undistort
    h,  w = img.shape[:2]
    newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
    dst = ...
    #
    # crop the image
    x, y, w, h = roi
    dst = dst[y:y+h, x:x+w]
    cvHelper.image(dst,"calibresult "+fname)


