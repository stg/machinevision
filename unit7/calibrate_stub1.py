#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 21:09:43 2019

"""
import numpy as np
import cv2 as cv
import glob
import cvHelper

cbrow = 12
cbcol = 11

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
world_points = np.zeros((cbrow * cbcol, 3), np.float32)
world_points[:, :2] = np.mgrid[0:cbcol, 0:cbrow].T.reshape(-1, 2)

# Arrays to store object points and image points from all the images.
list_3d_points = [] # 3d point in real world space
list_2d_points = [] # 2d points in image plane.

fname='./calib_example/Image14.tif'

img = cv.imread(fname,0)

# Find the chess board corners
ret, corners = ...

list_3d_points.append(world_points)
list_2d_points.append(corners)

# Draw and display the corners
img_color=cv.cvtColor(img,cv.COLOR_GRAY2RGB)
cv.drawChessboardCorners(img_color, (cbcol, cbrow), corners, ret)        
cvHelper.image(img_color, fname)

